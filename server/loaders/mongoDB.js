import config from '../config/index';
import mongoose from 'mongoose';

export default async()  => {

    const mongo = await mongoose
        .connect(
          config.databaseURL,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }
        );

    return mongo.connection.db;
}