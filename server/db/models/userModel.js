import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    birthday: Date,
    age: Number,
    city: String,
    position: String,
    isMarried: Boolean,
    created: Date,
    updated: Date
});

export default mongoose.model('User', userSchema);