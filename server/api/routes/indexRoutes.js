import {Router} from 'express';

const route = Router();

export default (app) => {

    app.use('/', route);

    route.get('/',
        async (request, response, next) => {
            response.send('Everything is ok!');
        }
    );


    /**
     * Healthy check endpoints
     */
    app.get('/status', (request,response) => {

        response
            .status(200)
            .json({
                status: 200,
                message: 'Server is up and running'
            })
            .end();
    });
}