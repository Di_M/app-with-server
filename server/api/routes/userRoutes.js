import {Router} from 'express';
import User from '../../db/models/userModel';
import bodyParser from 'body-parser';

const route = Router();

export default (app) => {
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    app.use('/user', route);


    // GET all collection from DB
    route.get('/',
        async (request, response, next) => {

            const list = await User.find();

            if(!list) {
                const messageRequest = {
                    status: 204,
                    message: "Sorry you dont have any data"
                };

                return response.json(messageRequest);
            }

            return response.json({user:list});
        }
    );


    // POST new User to DB collection
    route.post('/',
        async (request, response) => {
            const myData = new User(request.body);
            myData.save()
                .then(item => {
                    return response.json({user:[item]});
                });
        }
    );


    //DELETE User from DB collection by _id
    route.delete('/',
        async (request, response) => {
            const id = request.body.id;

            User.deleteOne({_id: id})
                .then(item => {
                    const messageRequest = {
                        status: 204,
                        message: `${JSON.stringify(item._id)} was deleted`
                    };

                    return response.json(messageRequest);
                });
        }
    );


    // UPDATE User in DB collection
    route.put('/',
        async (request, response) => {
            const data = request.body;

            User.updateOne({_id: data._id}, {$set: data})
                .then(item => {
                    return response.json({user:[item]});
                });
        }
    );
}
