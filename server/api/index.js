import {Router} from 'express';
import indexRoutes from './routes/indexRoutes';
import userRoutes from './routes/userRoutes';

export default () => {
    const app = Router();

    indexRoutes(app);
    userRoutes(app);

    return app;
}