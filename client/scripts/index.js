const btnGet = document.querySelector('.btn_getData');
const btnPost = document.querySelector('.btn_postData');
const btnDelete = document.querySelector('.btn_deleteData');
const btnPut = document.querySelector('.btn_putData');

const SERVER = 'http://localhost:3008/user';
const user = {
    firstName: 'Fill',
    lastName: 'String',
    birthday: new Date('2012-02-04'),
    age: '8',
    city: 'Chernihiv',
    position: 'child',
    isMarried: false,
    created: Date.now(),
    updated: Date.now()
};
const id = 2;
let dataFromServer = [];


const createPoint = (data) => {

    const point = document.createElement('p');
    point.textContent = data;
    return point;
};

const displayData = function (data) {

    if (data.status === 204) {
        const divData = document.createElement('div');
        divData.textContent = JSON.stringify(data.message);
        document.body.appendChild(divData);
    } else {
        console.log('Response -->', data.user);
        data.user.forEach(item => {
            const divData = document.createElement('div');

            if(!dataFromServer.includes(item)) {
                dataFromServer.push(item);
            }

            divData.appendChild(createPoint(item.firstName));
            divData.appendChild(createPoint(item.lastName));
            divData.appendChild(createPoint(new Date(item.birthday)));
            divData.appendChild(createPoint(item.age));
            divData.appendChild(createPoint(item.city));
            divData.appendChild(createPoint(item.position));

            document.body.appendChild(divData);
        });
    }
};

const displayError = (error) => {
    const divData = document.createElement('div');
    divData.textContent = error;
    divData.style.color = 'red';
    document.body.appendChild(divData);
};

const getDataList = async function(url) {
    return await fetch(url)
        .then(resp => resp.json())
        .then(result => result);
};

const postSortData = async function(url, dataObj) {

    return await fetch(url, {

        method: 'POST',
        body: JSON.stringify(dataObj),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(resp => resp.json());
};

const deleteData = async function(url, id) {

    return await fetch(url, {
        method: 'DELETE',
        body: JSON.stringify({id: id}),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(resp => resp.json());
};

const putData = async function(url, data) {

    return await fetch(url, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(resp => resp.json());
};



//Buttons events

btnGet.addEventListener('click', async function () {

    const dataRes = await getDataList(SERVER);

    displayData(dataRes);
});

btnPost.addEventListener('click', async function () {

    const dataRes = await postSortData(`${SERVER}`, user);

    displayData(dataRes);
});

btnDelete.addEventListener('click', async function() {

    if (dataFromServer.length > 0) {
        const dataRes = await deleteData(`${SERVER}`, dataFromServer[0]._id);

        displayData(dataRes);
    } else {
        const messageError = 'You have to get data before deleting';

        displayError(messageError);
    }

});

btnPut.addEventListener('click', async function() {

    if (dataFromServer.length > 0) {
        dataFromServer[0].firstName = 'Crisssssssssspo';

        const dataRes = await putData(`${SERVER}`, dataFromServer[0]);

        displayData(dataRes);
    } else {
        const messageError = `You have to get data before PUTTING`;

        displayError(messageError);
    }

});

